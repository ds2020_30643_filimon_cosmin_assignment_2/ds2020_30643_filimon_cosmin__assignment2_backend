package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.UserService;

@RestController
@CrossOrigin
@RequestMapping(value = "/userlogin")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public User getUser(@RequestParam String username, @RequestParam String password) {
        return userService.getUser(username, password);
    }
}
