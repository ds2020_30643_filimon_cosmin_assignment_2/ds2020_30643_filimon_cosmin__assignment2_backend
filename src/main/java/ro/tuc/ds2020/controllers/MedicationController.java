package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.services.MedicationService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public List<MedicationDTO> findAll() {
        return medicationService.findAll();
    }

    @PostMapping()
    public Integer insertMedication(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.insert(medicationDTO);
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Integer id) {
        return medicationService.findMedicationById(id);
    }

    @PutMapping()
    public Integer updateMedication(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.update(medicationDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody MedicationDTO medicationDTO) {
        medicationService.delete(medicationDTO);
    }
}
