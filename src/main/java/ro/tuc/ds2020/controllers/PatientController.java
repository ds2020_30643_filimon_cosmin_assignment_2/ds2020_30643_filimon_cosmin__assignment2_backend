package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientsCaregiversDTO;
import ro.tuc.ds2020.dtos.PatientsMedPlansDTO;
import ro.tuc.ds2020.services.PatientService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping()
    public List<PatientsCaregiversDTO> findAll() {
        return patientService.findAll();
    }

    @PostMapping()
    public Integer insertPatient(@RequestBody PatientsCaregiversDTO patientsCaregiversDTO) {
        return patientService.insert(patientsCaregiversDTO);
    }

    @GetMapping(value = "/{id}")
    public PatientsMedPlansDTO findById(@PathVariable("id") Integer id) {
        return patientService.findPatientById(id);
    }

    @PutMapping()
    public Integer updatePerson(@RequestBody PatientsCaregiversDTO patientsCaregiversDTO) {
        return patientService.update(patientsCaregiversDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody PatientDTO patientDTO) {
        patientService.delete(patientDTO);
    }
}
