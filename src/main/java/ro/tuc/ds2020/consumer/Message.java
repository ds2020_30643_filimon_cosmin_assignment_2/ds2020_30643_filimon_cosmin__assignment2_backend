package ro.tuc.ds2020.consumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ro.tuc.ds2020.entities.PatientActivity;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Message implements Serializable {

    private Integer caregiverId;
    private MessageType type;
    private PatientActivity patientActivity;

    public Integer getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(Integer caregiverId) {
        this.caregiverId = caregiverId;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public PatientActivity getPatientActivity() {
        return patientActivity;
    }

    public void setPatientActivity(PatientActivity patientActivity) {
        this.patientActivity = patientActivity;
    }
}
