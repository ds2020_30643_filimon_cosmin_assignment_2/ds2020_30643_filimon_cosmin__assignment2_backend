package ro.tuc.ds2020.consumer;

public enum  MessageType {
    USER_JOINED, TEXT_MESSAGE, USER_LEFT, USER_JOINED_ACK
}
