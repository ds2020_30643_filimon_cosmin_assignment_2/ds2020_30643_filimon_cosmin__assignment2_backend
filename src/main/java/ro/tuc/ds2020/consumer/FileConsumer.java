package ro.tuc.ds2020.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.java_websocket.WebSocket;
import ro.tuc.ds2020.entities.PatientActivity;
import org.json.simple.JSONObject;

import java.util.Set;

public class FileConsumer {

    private final static String QUEUE_NAME = "hello";
    private Set<WebSocket> conns;
    private long bathroom = 0;

    public FileConsumer(Set<WebSocket> conns) {
        this.conns = conns;
    }

    public void consume() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");

            PatientActivity patientActivity = new ObjectMapper().readValue(delivery.getBody(), PatientActivity.class);



            System.out.println(" [x] Received '" + message + "'");

            long hours = (patientActivity.getEnd() - patientActivity.getStart()) / 1000 / 60 / 60;
            long minutes = ((patientActivity.getEnd() - patientActivity.getStart()) / 1000 / 60) % 60;
            long seconds = ((patientActivity.getEnd() - patientActivity.getStart()) / 1000) % 60;
            System.out.println(patientActivity.getActivity() + " " + hours + "h " + minutes + "min " + seconds + "sec");

            long time = patientActivity.getEnd() - patientActivity.getStart();

            if(patientActivity.getActivity().equals("Toileting") || patientActivity.getActivity().equals("Grooming") || patientActivity.getActivity().equals("Showering")) {
                bathroom += time;
            } else {
                bathroom = 0;
            }

            /* Sending the message to client */

            JSONObject alertMessage = new JSONObject();
            alertMessage.put("patient_id", patientActivity.getPatient_id());

            boolean isPatientOk = true;
            if(patientActivity.getActivity().equals("Sleeping") && time > 1000 * 60 * 60 * 1) {
                alertMessage.put("message", "Patient " + patientActivity.getPatient_id() + " is sleeping for too long!");
                isPatientOk = false;
            } else if(patientActivity.getActivity().equals("Leaving") && time > 1000 * 60 * 60 * 2) {
                alertMessage.put("message", "Patient " + patientActivity.getPatient_id() + " is gone for too long!");
                isPatientOk = false;
            } else if(bathroom > 1000 * 60 * 10) {
                alertMessage.put("message", "Patient " + patientActivity.getPatient_id() + " is has stayed in bathroom more than usual!");
                isPatientOk = false;
            }

            if(!isPatientOk) {
                ObjectMapper mapper = new ObjectMapper();
                for (WebSocket sock : conns) {
                    sock.send(String.valueOf(alertMessage));
                }
            }



            /* --------------------------------------------------- */

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }


}
