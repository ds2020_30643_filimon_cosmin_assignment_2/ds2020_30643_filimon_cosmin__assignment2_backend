package ro.tuc.ds2020.dtos;

import java.util.List;

public class CaregiversPatientsDTO {
    private Integer id;
    private String name;
    private String birthdate;
    private String gender;
    private String address;
    private List<PatientsMedPlansDTO> patients;

    public CaregiversPatientsDTO() {
    }

    public CaregiversPatientsDTO(Integer id, String name, String birthdate, String gender, String address, List<PatientsMedPlansDTO> patients) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PatientsMedPlansDTO> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientsMedPlansDTO> patients) {
        this.patients = patients;
    }
}
