package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.MedicationPlan;

public class MedicationPlanBuilder {
    public MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan) {
        if(medicationPlan == null) return null;
        return new MedicationPlanDTO(
                medicationPlan.getId(),
                medicationPlan.getIntakeIntervals(),
                medicationPlan.getStartDate(),
                medicationPlan.getEndDate()
        );
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO) {
        if(medicationPlanDTO == null) return null;
        return new MedicationPlan(
                medicationPlanDTO.getIntakeIntervals(),
                medicationPlanDTO.getStartDate(),
                medicationPlanDTO.getEndDate()
        );
    }
}
