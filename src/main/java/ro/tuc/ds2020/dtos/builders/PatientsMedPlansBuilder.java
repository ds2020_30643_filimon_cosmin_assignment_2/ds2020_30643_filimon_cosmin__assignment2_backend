package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientsMedPlansDTO;
import ro.tuc.ds2020.entities.Patient;

import java.util.ArrayList;
import java.util.List;

public class PatientsMedPlansBuilder {
    public PatientsMedPlansBuilder() {
    }

    public static PatientsMedPlansDTO generateDTOFromEntity(Patient patient) {
        if(patient == null) return null;

        List<MedicationPlanDTO> medicationPlanDTOS = new ArrayList<>();
        patient.getMedicationPlans().forEach(medicationPlan -> {
            MedicationPlanDTO med = MedicationPlanBuilder.generateDTOFromEntity(medicationPlan);
            medicationPlanDTOS.add(med);
        });

        return new PatientsMedPlansDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getAddress(),
                medicationPlanDTOS
        );
    }
}
