package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.entities.Caregiver;

public class CaregiverBuilder {
    public CaregiverBuilder() {
    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver) {
        if(caregiver == null) return null;
        return new CaregiverDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthdate(),
                caregiver.getGender(),
                caregiver.getAddress()
        );
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO) {
        if (caregiverDTO == null) return null;
        return new Caregiver(
                caregiverDTO.getName(),
                caregiverDTO.getBirthdate(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress()
        );
    }
}
