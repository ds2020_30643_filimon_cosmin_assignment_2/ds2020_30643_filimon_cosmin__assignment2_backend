package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedPlansPatientsMedicationsDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;

import java.util.ArrayList;
import java.util.List;

public class MedPlansPatientsMedicationsBuilder {
    public MedPlansPatientsMedicationsBuilder() {
    }

    public static MedPlansPatientsMedicationsDTO generateDTOFromEntity(MedicationPlan medicationPlan) {
        if(medicationPlan == null) return null;

        PatientDTO patientDTO = PatientBuilder.generateDTOFromEntity(medicationPlan.getPatient());

        List<MedicationDTO> medicationDTOS = new ArrayList<>();
        medicationPlan.getMedications().forEach(medication -> {
            MedicationDTO med = MedicationBuilder.generateDTOFromEntity(medication);
            medicationDTOS.add(med);
        });

        return new MedPlansPatientsMedicationsDTO(
                medicationPlan.getId(),
                medicationPlan.getIntakeIntervals(),
                medicationPlan.getStartDate(),
                medicationPlan.getEndDate(),
                patientDTO,
                medicationDTOS
        );
    }

    public static MedicationPlan generateEntityFromDTO(MedPlansPatientsMedicationsDTO medPlansPatientsMedicationsDTO) {
        if(medPlansPatientsMedicationsDTO == null) return null;

        Patient patient = PatientBuilder.generateEntityFromDTO(medPlansPatientsMedicationsDTO.getPatient());
        patient.setId(medPlansPatientsMedicationsDTO.getPatient().getId());

        List<Medication> medications = new ArrayList<>();
        medPlansPatientsMedicationsDTO.getMedications().forEach(medicationDTO -> {
            Medication med = MedicationBuilder.generateEntityFromDTO(medicationDTO);
            med.setId(medicationDTO.getId());
            medications.add(med);
        });

        return new MedicationPlan(
                medPlansPatientsMedicationsDTO.getIntakeIntervals(),
                medPlansPatientsMedicationsDTO.getStartDate(),
                medPlansPatientsMedicationsDTO.getEndDate(),
                medications,
                patient
        );
    }
}
