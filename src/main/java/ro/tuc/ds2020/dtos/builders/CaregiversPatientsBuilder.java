package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiversPatientsDTO;
import ro.tuc.ds2020.dtos.PatientsMedPlansDTO;
import ro.tuc.ds2020.entities.Caregiver;

import java.util.ArrayList;
import java.util.List;

public class CaregiversPatientsBuilder {
    public CaregiversPatientsBuilder() {
    }

    public static CaregiversPatientsDTO generateDTOFomEntity(Caregiver caregiver) {
        if(caregiver == null) return null;

        List<PatientsMedPlansDTO> patientsMedPlansDTOS = new ArrayList<>();
        caregiver.getPatients().forEach(patient -> {
            PatientsMedPlansDTO patientDTO = PatientsMedPlansBuilder.generateDTOFromEntity(patient);
            patientsMedPlansDTOS.add(patientDTO);
        });

        return new CaregiversPatientsDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthdate(),
                caregiver.getGender(),
                caregiver.getAddress(),
                patientsMedPlansDTOS
        );
    }
}
