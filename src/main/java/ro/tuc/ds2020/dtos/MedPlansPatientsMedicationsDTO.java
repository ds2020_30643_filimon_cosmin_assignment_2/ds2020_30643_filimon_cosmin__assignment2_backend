package ro.tuc.ds2020.dtos;

import java.util.List;

public class MedPlansPatientsMedicationsDTO {
    private Integer id;
    private String intakeIntervals;
    private String startDate;
    private String endDate;
    private PatientDTO patient;
    private List<MedicationDTO> medications;

    public MedPlansPatientsMedicationsDTO() {
    }

    public MedPlansPatientsMedicationsDTO(Integer id, String intakeIntervals, String startDate, String endDate, PatientDTO patient, List<MedicationDTO> medications) {
        this.id = id;
        this.intakeIntervals = intakeIntervals;
        this.startDate = startDate;
        this.endDate = endDate;
        this.patient = patient;
        this.medications = medications;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public PatientDTO getPatient() {
        return patient;
    }

    public void setPatient(PatientDTO patient) {
        this.patient = patient;
    }

    public List<MedicationDTO> getMedications() {
        return medications;
    }

    public void setMedications(List<MedicationDTO> medications) {
        this.medications = medications;
    }
}
