package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.PatientActivity;
import ro.tuc.ds2020.repositories.PatientActivityRepository;

@Service
public class PatientActivityService {
    private final PatientActivityRepository patientActivityRepository;

    @Autowired
    public PatientActivityService(PatientActivityRepository patientActivityRepository) {
        this.patientActivityRepository = patientActivityRepository;
    }

    // Create medication
    public Integer insert(PatientActivity patientActivity) {
        // TODO: add fields validators
        // TODO: add something for duplicates idk

        return patientActivityRepository
                .save(patientActivity)
                .getId();
    }
}
