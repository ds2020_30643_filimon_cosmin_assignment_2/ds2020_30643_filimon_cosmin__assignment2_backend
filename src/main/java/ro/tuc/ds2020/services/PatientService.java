package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientsCaregiversDTO;
import ro.tuc.ds2020.dtos.PatientsMedPlansDTO;
import ro.tuc.ds2020.dtos.builders.PatientsCaregiversBuilder;
import ro.tuc.ds2020.dtos.builders.PatientsMedPlansBuilder;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<PatientsCaregiversDTO> findAll() {
        List<Patient> patients = patientRepository.findAll();

        return patients.stream()
                .map(PatientsCaregiversBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public Integer insert(PatientsCaregiversDTO patientWithCaregiverDTO) {


        return patientRepository
                .save(PatientsCaregiversBuilder.generateEntityFromDTO(patientWithCaregiverDTO))
                .getId();
    }

    public PatientsMedPlansDTO findPatientById(Integer id) {
        Optional<Patient> patient = patientRepository.findById(id);


        return PatientsMedPlansBuilder.generateDTOFromEntity(patient.get());
    }


    public Integer update(PatientsCaregiversDTO patientWithCaregiverDTO) {
        Optional<Patient> patient = patientRepository.findById(patientWithCaregiverDTO.getId());



        Patient updatedPatient = PatientsCaregiversBuilder.generateEntityFromDTO(patientWithCaregiverDTO);
        updatedPatient.setId(patientWithCaregiverDTO.getId());

        return patientRepository
                .save(updatedPatient)
                .getId();
    }


    public void delete(PatientDTO patientDTO) {
        patientRepository.deleteById(patientDTO.getId());
    }
}
