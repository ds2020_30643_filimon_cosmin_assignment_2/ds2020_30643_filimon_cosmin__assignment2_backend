package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedPlansPatientsMedicationsDTO;
import ro.tuc.ds2020.dtos.builders.MedPlansPatientsMedicationsBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public List<MedPlansPatientsMedicationsDTO> findAll() {
        List<MedicationPlan> medications = medicationPlanRepository.findAll();

        return medications.stream()
                .map(MedPlansPatientsMedicationsBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public Integer insert(MedPlansPatientsMedicationsDTO medPlansPatientsMedicationsDTO) {

        MedicationPlan m = MedPlansPatientsMedicationsBuilder.generateEntityFromDTO(medPlansPatientsMedicationsDTO);

        return medicationPlanRepository
                .save(MedPlansPatientsMedicationsBuilder.generateEntityFromDTO(medPlansPatientsMedicationsDTO))
                .getId();
    }
}
