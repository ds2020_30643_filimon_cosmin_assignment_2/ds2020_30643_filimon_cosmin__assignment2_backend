package ro.tuc.ds2020.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "medicationplan")
public class MedicationPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "intakeintervals", nullable = false)
    private String intakeIntervals;

    @Column(name = "startdate", nullable = false)
    private String startDate;

    @Column(name = "enddate", nullable = false)
    private String endDate;

    @ManyToMany(cascade=CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(
            joinColumns = @JoinColumn(name = "medicationplan_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id")
    )
    private List<Medication> medications;

    @ManyToOne()
    @JoinColumn(name = "patient_id")
    private Patient patient;

    public MedicationPlan() {
    }

    public MedicationPlan(String intakeIntervals, String startDate, String endDate) {
        this.intakeIntervals = intakeIntervals;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public MedicationPlan(String intakeIntervals, String startDate, String endDate, List<Medication> medications, Patient patient) {
        this.intakeIntervals = intakeIntervals;
        this.startDate = startDate;
        this.endDate = endDate;
        this.medications = medications;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<Medication> getMedications() {
        return medications;
    }

    public void setMedications(List<Medication> medications) {
        this.medications = medications;
    }
}
