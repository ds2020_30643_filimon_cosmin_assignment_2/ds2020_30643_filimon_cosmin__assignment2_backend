package ro.tuc.ds2020.entities;

import javax.persistence.*;

@Entity
@Table(name = "patientactivity")
public class PatientActivity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "patient_id", nullable = false)
    private Integer patient_id;

    @Column(name = "activity", nullable = false)
    private String activity;

    @Column(name = "start", nullable = false)
    private long start;

    @Column(name = "eend", nullable = false)
    private long end;

    public PatientActivity() {
    }

    public PatientActivity(Integer patient_id, String activity, long start, long end) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "PatientActivity{" +
                "id=" + id +
                ", patient_id=" + patient_id +
                ", activity='" + activity + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}
